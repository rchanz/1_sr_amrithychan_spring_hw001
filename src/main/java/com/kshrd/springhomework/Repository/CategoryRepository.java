package com.kshrd.springhomework.Repository;

import com.kshrd.springhomework.Repository.Model.CategoryDto;
import com.kshrd.springhomework.Repository.Provider.CategoryProvider;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @InsertProvider(value = CategoryProvider.class, method = "insert")
    boolean insert(CategoryDto category);

    @SelectProvider(value = CategoryProvider.class, method = "selectAll")
    List<CategoryDto> selectAll();

    @DeleteProvider(value = CategoryProvider.class, method = "delete")
    void delete(int id);

    @UpdateProvider(value = CategoryProvider.class, method = "update")
    boolean update(int id, CategoryDto category);


}

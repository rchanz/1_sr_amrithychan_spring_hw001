package com.kshrd.springhomework.Repository;

import com.kshrd.springhomework.Repository.Model.BookDto;
import com.kshrd.springhomework.Repository.Model.CategoryDto;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {

    @Select("SELECT * FROM tb_book ORDER BY id ASC")
    @Results({
            @Result(
                    property = "category",
                    javaType = CategoryDto.class,
                    column = "category_id",
                    one = @One(select = "getCategory")
            )
    })
    List<BookDto> findAll();

    @Select("SELECT * FROM tb_book WHERE id = #{id}")
    @Results({
            @Result(
                    property = "category",
                    javaType = CategoryDto.class,
                    column = "category_id",
                    one = @One(select = "getCategory")
            )
    })
    BookDto findById(int id);

    @Insert("INSERT INTO tb_book (title, description, author, thumbnail, category_id) " +
            "VALUES (#{title}, #{description}, #{author}, #{thumbnail}, #{category.id})")
    boolean insert(BookDto bookDto);

    @Delete("DELETE FROM tb_book WHERE id = #{id}")
    boolean delete(int id);

    @Update("UPDATE tb_book set title = #{bookDto.title}, " +
            "description = #{bookDto.description}, " +
            "author = #{bookDto.author}, thumbnail = #{bookDto.thumbnail}, category_id = #{bookDto.category.id} WHERE id = #{id} ")
    boolean update(int id, BookDto bookDto);


    @Select("SELECT * FROM tb_category WHERE id=#{category_id}")
    CategoryDto getCategory(int categoryId);

}

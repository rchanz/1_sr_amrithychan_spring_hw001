package com.kshrd.springhomework.Repository.Provider;

import com.kshrd.springhomework.Repository.Model.CategoryDto;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String selectAll(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_category");
        }}.toString();
    }
    public String insert(){
        return new SQL(){{
            INSERT_INTO("tb_category");
            VALUES("title", "#{title}");
        }}.toString();
    }

    public String delete(int id){
        return new SQL(){{
            DELETE_FROM("tb_category");
            if(id>0)
                WHERE("id=#{id}");
        }}.toString();
    }

    public String update(int id, CategoryDto category){
        return new SQL(){{
            UPDATE("tb_category");
            SET("title=#{category.title}");
            WHERE("id=#{id}");
        }}.toString();
    }


}

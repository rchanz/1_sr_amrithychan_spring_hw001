package com.kshrd.springhomework.Service.Impl;

import com.kshrd.springhomework.Repository.CategoryRepository;
import com.kshrd.springhomework.Repository.Model.CategoryDto;
import com.kshrd.springhomework.Service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoryDto insert(CategoryDto category) {
        boolean isInserted = categoryRepository.insert(category);
        if(isInserted)
            return category;
        else
            return null;
    }

    @Override
    public List<CategoryDto> selectAll() {
        return categoryRepository.selectAll();
    }

    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }

    @Override
    public CategoryDto update(int id, CategoryDto category) {
        boolean isUpdated = categoryRepository.update(id, category);
        if(isUpdated)
            return category;
        else
            return null;
    }
}

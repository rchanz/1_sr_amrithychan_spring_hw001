package com.kshrd.springhomework.Service.Impl;

import com.kshrd.springhomework.Repository.BookRepository;
import com.kshrd.springhomework.Repository.Model.BookDto;
import com.kshrd.springhomework.Service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookDto insert(BookDto bookDto) {
        boolean isInserted = bookRepository.insert(bookDto);
        if(isInserted){
            return bookDto;
        } else {
            return null;
        }
    }

    @Override
    public List<BookDto> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public BookDto findById(int id) {
        return bookRepository.findById(id);
    }

    @Override
    public boolean deleteById(int id) {
        return bookRepository.delete(id);
    }

    @Override
    public BookDto updateById(int id, BookDto bookDto) {
        boolean isUpdated = bookRepository.update(id, bookDto);
        return isUpdated ? bookDto : null;
    }
}

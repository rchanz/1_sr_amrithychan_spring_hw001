package com.kshrd.springhomework.Service;

import com.kshrd.springhomework.Repository.Model.CategoryDto;

import java.util.List;

public interface CategoryService {

    CategoryDto insert(CategoryDto category);
    List<CategoryDto> selectAll();
    void delete (int id);
    CategoryDto update (int id, CategoryDto category);

}

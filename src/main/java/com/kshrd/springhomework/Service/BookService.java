package com.kshrd.springhomework.Service;

import com.kshrd.springhomework.Repository.Model.BookDto;

import java.util.List;

public interface BookService {
    BookDto insert(BookDto bookDto);
    List<BookDto> findAll();
    BookDto findById(int id);
    boolean deleteById(int id);
    BookDto updateById(int id, BookDto bookDto);
}

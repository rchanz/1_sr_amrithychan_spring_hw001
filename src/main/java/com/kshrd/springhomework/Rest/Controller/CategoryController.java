package com.kshrd.springhomework.Rest.Controller;

import com.kshrd.springhomework.Repository.Model.CategoryDto;
import com.kshrd.springhomework.Rest.Request.CategoryRequestModel;
import com.kshrd.springhomework.Rest.Response.BaseApiResponse;
import com.kshrd.springhomework.Rest.Response.GetApiResponse;
import com.kshrd.springhomework.Service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoryController {
    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/category")
    public ResponseEntity<GetApiResponse<List<CategoryDto>>> selectAll () {
        GetApiResponse<List<CategoryDto>> apiResponse = new GetApiResponse<>();

        List<CategoryDto> categoryList = categoryService.selectAll();

        apiResponse.setMessage("All categories have been found");
        apiResponse.setData(categoryList);
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }


    @PostMapping("/category")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> insert (@RequestBody CategoryRequestModel request){
        BaseApiResponse<CategoryRequestModel> apiResponse = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        CategoryDto category = mapper.map(request, CategoryDto.class);

        categoryService.insert(category);

        apiResponse.setMessage("Category has been added!");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }

    @PutMapping("/category/{categoryId}")
    public ResponseEntity<BaseApiResponse<String>> update (@PathVariable int categoryId,
                                                           @RequestBody CategoryRequestModel requestModel){
        BaseApiResponse<String> apiResponse = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        CategoryDto category = mapper.map(requestModel, CategoryDto.class);

        CategoryDto action = categoryService.update(categoryId, category);

        apiResponse.setMessage("Category has been updated!");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }

    @DeleteMapping("/category/{categoryId}")
    public ResponseEntity<BaseApiResponse<String>> delete(@PathVariable int categoryId) {
        BaseApiResponse<String> apiResponse = new BaseApiResponse<>();
        categoryService.delete(categoryId);

        apiResponse.setMessage("Category has been deleted");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }

}

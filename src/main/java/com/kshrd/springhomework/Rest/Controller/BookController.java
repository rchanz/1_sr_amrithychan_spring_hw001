package com.kshrd.springhomework.Rest.Controller;

import com.kshrd.springhomework.Repository.Model.BookDto;
import com.kshrd.springhomework.Rest.Request.BookRequestModel;
import com.kshrd.springhomework.Rest.Response.BaseApiResponse;
import com.kshrd.springhomework.Rest.Response.GetApiResponse;
import com.kshrd.springhomework.Service.Impl.BookServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {

    private BookServiceImpl bookService;

    @Autowired
    public void setBookService(BookServiceImpl bookService) {
        this.bookService = bookService;
    }

    //find all dem books
    @GetMapping("/book")
    public ResponseEntity<GetApiResponse<List<BookRequestModel>>> findAll(@RequestParam(value = "id", required = false) String bookId){
        ModelMapper mapper = new ModelMapper();
        GetApiResponse<List<BookRequestModel>> response =
                new GetApiResponse<>();

        System.out.println(bookId);

        List<BookDto> bookDtoList = bookService.findAll();
        List<BookRequestModel> books = new ArrayList<>();

        for (BookDto bookDto : bookDtoList) {
            books.add(mapper.map(bookDto, BookRequestModel.class));
        }

        response.setMessage("You have found all the books!");
        response.setData(books);
        response.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(response);
    }

    //find a book by ID
    @GetMapping("/book/{bookId}")
    public ResponseEntity<GetApiResponse> findById(@PathVariable int bookId){
        GetApiResponse<BookRequestModel> response =
                new GetApiResponse<>();
        ModelMapper mapper = new ModelMapper();


        BookDto bookDto = bookService.findById(bookId);
        BookRequestModel res = new BookRequestModel();

        res = mapper.map(bookDto, BookRequestModel.class);

        response.setMessage("You have found the book with id " + bookId + "!");
        response.setData(res);
        response.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(response);
    }

    @PostMapping("/book")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> insert(@RequestBody BookRequestModel book){

        BaseApiResponse<BookRequestModel> res = new BaseApiResponse<>();

        ModelMapper modelMapper = new ModelMapper();
        BookDto bookDto = modelMapper.map(book, BookDto.class);
        bookService.insert(bookDto);

        //BookRequestModel response = modelMapper.map(result, BookRequestModel.class);

        res.setMessage("Entry added successfully");
        res.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(res);
    }

    @DeleteMapping("/book/{bookId}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> deleteById(@PathVariable int bookId){
        BaseApiResponse<BookRequestModel> res = new BaseApiResponse<>();
        bookService.deleteById(bookId);
        res.setMessage("Entry deleted!");
        res.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(res);
    }

    @PutMapping("/book/{bookId}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> editById(@PathVariable int bookId,
                                                                     @RequestBody BookRequestModel request){
        BaseApiResponse<BookRequestModel> apiResponse = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        BookDto book = mapper.map(request, BookDto.class);

        BookDto action = bookService.updateById(bookId, book);
        BookRequestModel response = mapper.map(action, BookRequestModel.class);

        apiResponse.setMessage("Book has been updated!");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }
}

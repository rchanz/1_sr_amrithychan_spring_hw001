package com.kshrd.springhomework.Rest.Request;

import com.kshrd.springhomework.Repository.Model.CategoryDto;

public class BookRequestModel {
    private int id;
    private String title;
    private String description;
    private String author;
    private String thumbnail;
    private CategoryDto category;

    public BookRequestModel(){}

    public BookRequestModel(String title, String description, String author, String thumbnail, CategoryDto category) {
        this.title = title;
        this.description = description;
        this.author = author;
        this.thumbnail = thumbnail;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public CategoryDto getCategory() {
        return category;
    }
}
